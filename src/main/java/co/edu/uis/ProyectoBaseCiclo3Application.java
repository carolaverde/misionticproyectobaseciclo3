package co.edu.uis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoBaseCiclo3Application {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoBaseCiclo3Application.class, args);
	}

}
