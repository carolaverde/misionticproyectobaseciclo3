package co.edu.uis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.validation.Valid;

import co.edu.uis.model.User;
import co.edu.uis.repository.UserRepository;

@Controller
@RequestMapping(path = "/user")
public class UserController {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@RequestMapping(method = RequestMethod.POST, value = "/signup")
	public String saveUser(@Valid User user, RedirectAttributes redirAttrs, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			redirAttrs.addFlashAttribute("message", bindingResult.getFieldErrors().toString());
		} else {
			user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
			userRepository.save(user);
			redirAttrs.addFlashAttribute("message", "User created");
		}

		return "redirect:/login";
	}

	@GetMapping("/signup")
	public void showSignUpView() {
	}

}
